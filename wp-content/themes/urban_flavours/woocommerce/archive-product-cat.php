<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
global $wp_query;

?>
    <!-- /.categories-product start  -->
    <section class="categories-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 page-name ">
                    <p>Home / All Products</p>
                    <?php
                    global $wp_query;
                    $selected_trm_name = $wp_query->queried_object->name;
                    if($wp_query->queried_object->parent){
                        $trm = get_term_by( 'id', $wp_query->queried_object->parent, $wp_query->queried_object->taxonomy );
                        $selected_trm_name = $trm->name;
                    }
                    $args = array(
                        'orderby'           => 'id',
                        'order'             => 'ASC',
                        'parent'            => 0,
                        'hide_empty'            => false

                    );
                    $terms = get_terms('product_cat', $args);
                    if(!empty($terms)):?>
                    <header class="woocommerce-products-header">
                        <h6 class="woocommerce-products-header__title page-title">Categories</h6>
                    </header>
                    <ul class="list-unstyled cat-list category_selection">
                        <?php
                        foreach ($terms as $term):;
                            if($term->name == 'Uncategorized')
                                continue;
                            $pr_term_id = $term->term_id;
                            $args = array(
                                'orderby'           => 'id',
                                'order'             => 'ASC',
                                'parent'            => $pr_term_id,
                                'hide_empty'            => false
                            );
                            $sub_categories = get_terms('product_cat', $args);
                            ?>
                            <li <?php if($term->name === $selected_trm_name){?>class="active"<?php };?>>
                                <p class="m-0">
                                    <a class="acc_trigger d-inline-block" href="<?php echo get_term_link($term);?>">
                                        <?php echo $term->name;?> (<?php echo $term->count;?>)
                                    </a>
                                    <?php if(!empty($sub_categories)):?>
                                        <i class="pe-lg pe-va float-right pe-2x mt-3 <?php if($term->name === $selected_trm_name){?>active pe-7s-angle-up<?php }else{?>pe-7s-angle-down<?php };?>"></i>
                                    <?php endif;?>
                                </p>
                                <?php if(!empty($sub_categories)):?>
                                    <div class="acc_container" >
                                        <div class="card card-body ">
                                            <?php foreach ($sub_categories as $term):?>
                                                <a href="<?php echo get_term_link($term);?>" <?php if($wp_query->queried_object->parent && $term->name === $wp_query->queried_object->name):?>class="active" <?php endif;?>> <?php echo $term->name;?></a>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                <?php endif;?>
                            </li>
                        <?php endforeach;?>
                    </ul>
                    <?php endif;?>
                    <div class="d-none hide-con">
                        <?php
                        $field_name = "strength";
                        $field = get_field_object($field_name);
                        if(!empty($field['choices'])):
                            ?>
                            <header class="woocommerce-products-header">
                                <h6 class="woocommerce-products-header__title page-title">STRENGTH</h6>
                            </header>

                            <ul class="list-unstyled cat-list pb-70 strength_selection_cat">
                                <?php foreach ($field['choices'] as $key => $value):?>
                                    <li>
                                        <a class="acc_trigger" data-taxonomy="<?php echo $wp_query->queried_object->taxonomy;?>" data-trmid="<?php echo $wp_query->queried_object->term_id;?>"><?php echo $value;?></a>
                                    </li>
                                    <?php $i++;endforeach;?>
                            </ul>
                        <?php endif;?>

                        <?php
                        $first_val = get_option('first_val');
                        $last_val = get_option('last_val');
                        if($first_val === false){
                            $first_val = 5;
                            $last_val = 35;
                        }
                        ?>
                        <header class="woocommerce-products-header">
                            <h6 class="woocommerce-products-header__title page-title">Price</h6>
                        </header>
                        <div class="price-filter ">
                            <div class="slider-wrapper black-text">
                                <input class="input-range"  data-slider-id='ex12cSlider' type="text" data-slider-step="1" data-slider-value="<?php echo $first_val . ',' . $last_val;?>" data-slider-min="0" data-slider-max="<?php echo $last_val;?>" data-slider-range="true" data-slider-tooltip_split="true" />
                            </div>
                            <p>$<span class="min-value"><?php echo $first_val;?> </span> - $<span class="max-value"><?php echo $last_val;?></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-12 text-center item-con">
                    <div class="full-wait wait-reg"></div>
                    <h6 class="green-text title text-uppercase"><?php woocommerce_page_title(); ?></h6>
                    <div class="shorting-container mb-3">
                        <div class="woocommerce-notices-wrapper"></div>

                        <!--<form class="woocommerce-ordering float-left mr-5 order-count" method="get">
                            <label> Show by : </label>
                            <select name="orderby" class="orderby">
                                <option value="menu_order" selected="selected">15</option>
                                <option value="menu_order" selected="selected">16</option>
                                <option value="menu_order" selected="selected">17</option>
                                <option value="menu_order" selected="selected">18</option>

                            </select>
                            <input type="hidden" name="paged" value="1">
                        </form>-->

                        <form class="sort-by float-left " method="get">
                            <label> Short by : </label>
                            <select id="order_by" name="order_by" class="orderby" data-taxonomy="<?php echo $wp_query->queried_object->taxonomy;?>" data-trmid="<?php echo $wp_query->queried_object->term_id;?>">
                                <option value="best-selling" selected="selected">Best Selling</option>
                                <option value="popularity">Popularity</option>
                                <option value="rating">Average rating</option>
                                <option value="latest"> Latest</option>
                                <option value="low-to-high">Price: Low to high</option>
                                <option value="high-to-low">Price: High to low</option>
                            </select>
                            <input type="hidden" name="paged" value="1">
                        </form>
                        <!--<p class="woocommerce-result-count float-lg-right float-md-right float-sm-left">Items 1 - 15 of 28 total</p>-->
                        <div class="clearfix"></div>
                    </div>
                    <?php
                    $args = array(
                        'post_type'             => 'product',
                        'post_status'           => 'publish',
                        'posts_per_page'        => 2,
                        'tax_query'             => array(
                            array(
                                'taxonomy'      => $wp_query->queried_object->taxonomy,
                                'field' => 'term_id',
                                'terms'         => $wp_query->queried_object->term_id,
                                'operator'      => 'IN'
                            )
                        )
                    );
                    $products = new WP_Query($args);

                    if ( $products->have_posts() ) :?>
                        <ul class="products columns-3 cat-items">
                            <?php
                            while ( $products->have_posts() ) : $products->the_post();
                                global $product;
                                ?>
                                <li class="post-<?php echo $product->get_id();?> product type-product status-publish entry product-type-<?php echo $product->get_type();?>">
                                    <a href="<?php echo $product->get_permalink();?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                        <?php echo $product->get_image('woocommerce_thumbnail');?>
                                        <h6 class="woocommerce-loop-product__title"><?php echo $product->get_name();?></h6>
                                        <?php if($product->get_type() == 'variable'):?>
                                            <span class="price"><span class="woocommerce-Price-amount amount">from <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span> <?php echo $product->get_price();?></span></span>
                                        <?php else:?>
                                            <span class="price"><?php echo $product->get_price_html();?></span>
                                        <?php endif;?>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                        <?php if($wp_query->queried_object->count > $products->post_count):?>
                        <a href="javascript:void(0);" class="btn load-more load-more2" data-trmid="<?php echo $wp_query->queried_object->term_id;?>" data-taxonomy="<?php echo $wp_query->queried_object->taxonomy;?>" data-dnoi="<?php echo $products->post_count;?>" data-curpageid="1">SHOW ALL <?php echo $wp_query->queried_object->count;?> PRODUCTS <i aria-hidden="true" class="arrow_carrot-2down d-block"></i></a>
                        <?php endif;?>
                    <?php else : ?>
                        <p><?php esc_html_e( 'Sorry, no product found.' ); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </section>
    <!-- /.categories-product end  -->
<?php

get_footer( 'shop' );
