<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

$catIds = $product->get_category_ids();
$category_name = [];
if (!empty($catIds)) {
    foreach ($catIds as $cat_id) {
        $trm = get_term_by('id', $cat_id, 'product_cat');
        if($trm->parent != 0) {
            $link = get_term_link($trm);
            $trm_name = $trm->name;
            $trm_id = $trm->term_id;
            $category_name[] = $trm->name;
            break;
        }
    }
}
if(empty($category_name)){
    $trm = get_term_by('id', $catIds[0], 'product_cat');
    $link = get_term_link($trm);
    $trm_name = $trm->name;
    $trm_id = $trm->term_id;
}

?>

<section class="product-details">
    <div class="container">
        <div class="row">
            <div class="col-10 m-auto">
                <nav class="woocommerce-breadcrumb"><a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) );?>">Shop</a>&nbsp;/&nbsp;<a href="<?php echo $link;?>"><?php echo $trm_name;?></a>&nbsp;/&nbsp;<a href=""><?php the_title();?></a></nav>
                <?php do_action( 'woocommerce_before_single_product' );?>
                <div id="product-<?php echo $product->get_id();?>" class="post-<?php echo $product->get_id();?> product type-product status-publish product-type-<?php echo $product->get_type();?>">
                    <?php
                    do_action( 'woocommerce_before_single_product_summary' );
                    ?>
                    <div class="summary entry-summary">
                        <div class="product_meta">
                            <span class="posted_in"> <a href="<?php echo $link;?>" rel="tag"><?php echo $trm_name;?></a></span>
                        </div>

                        <div class="title-box">
                            <h6 class="product_title entry-title float-lg-left float-md-none float-sm-none mb-lg-0 mb-md-2 mb-sm-2"><?php the_title();?></h6>
                            <div class="review-box float-lg-right float-md-none float-sm-none">
                                <?php
                                $rating_1 = $product->get_rating_count(1);
                                $rating_2 = $product->get_rating_count(2);
                                $rating_3 = $product->get_rating_count(3);
                                $rating_4 = $product->get_rating_count(4);
                                $rating_5 = $product->get_rating_count(5);
                                $total = ($rating_1 * 1) + ($rating_2 * 2) + ($rating_3 * 3) + ($rating_4 * 4) + ($rating_5 * 5);

                                if($total > 0){
                                    $avg = round($total / $product->get_review_count());
                                }else{
                                    $avg = 0;
                                }
                                ?>
                                <ul class="list-inline d-inline-block">
                                    <?php
                                    for($i = 1; $i <= 5; $i++){
                                        if(!$avg){
                                            echo '<li class="list-inline-item"><a href="javascript:void(0)"><i class="fa fa-star-o "></i></a></li>';
                                        }else{
                                            if($i <= $avg){
                                                echo '<li class="list-inline-item active"><a href="javascript:void(0)"><i class="fa fa-star-o "></i></a></li>';
                                            }else{
                                                echo '<li class="list-inline-item"><a href="javascript:void(0)"><i class="fa fa-star-o "></i></a></li>';
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                                <p class="d-inline-block">
                                    <?php echo $product->get_review_count();?> reviews
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <?php if($product->get_type() == 'variable'):?>
                            <p class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol();?></span><?php echo $product->get_price();?></span></p>
                        <?php else:?>
                            <p class="price"><?php echo $product->get_price_html();?></p>
                        <?php endif;?>
                        <?php $_product = $product;?>
                        <?php if($_product->get_type() == 'variable'):
                        $variations = $_product->get_available_variations();
                        if(!empty($variations)):
                            $attributes = $_product->get_variation_attributes();
                            $default = $_product->get_default_attributes();
                            foreach ($attributes as $key => $value):
                                $terms = wc_get_product_terms( $_product->get_id(), $key, array( 'fields' => 'all' ) );
                                $narr = [];
                                if(!empty($terms)){
                                    foreach ($terms as $term):
                                        $narr[$term->slug] = $term->name;
                                    endforeach;
                                }
                            endforeach;

                            $variation_id = cus_find_matching_product_variation( $_product, $default );
                            ?>
                            <ul class="list-inline variation">
                                <?php //foreach ($attributes as $key => $value):?>
                                <?php foreach ($variations as $key => $value):?>
                                    <?php if(is_array($value)):?>
                                        <?php
                                        $attr_arr=[];
                                        $attr_arr['pa_size'] = $value['attributes']['attribute_pa_size'];
                                        ?>
                                        <?php //foreach ($value as $itm => $val):?>
                                        <li class="list-inline-item <?php if(in_array($value['attributes']['attribute_pa_size'],$default)){?>active<?php };?>" id="vid-<?php echo $value['variation_id'];?>">
                                            <p data-variation_id="<?php echo $value['variation_id'];?>" data-product_id="<?php echo $_product->get_id();?>" data-attri="attribute_pa_size" data-attri_val="<?php echo $value['attributes']['attribute_pa_size'];?>" data-attribute="<?php echo htmlspecialchars(json_encode($attr_arr), ENT_QUOTES, 'UTF-8'); ?>"><?php echo $narr[$value['attributes']['attribute_pa_size']];?></p>
                                            <p data-variation_id="<?php echo $value['variation_id'];?>" data-product_id="<?php echo $_product->get_id();?>" data-attri="attribute_pa_size" data-attri_val="<?php echo $value['attributes']['attribute_pa_size'];?>" data-attribute="<?php echo htmlspecialchars(json_encode($attr_arr), ENT_QUOTES, 'UTF-8'); ?>"><?php echo get_woocommerce_currency_symbol();?><?php echo $value['display_price'];?></p>
                                        </li>
                                        <?php //endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </ul>

                            <form class="cart single_add_tc" action="" method="post" enctype="multipart/form-data">
                                <div class="quantity">
                                    <a href="javascript:void(0)" class="btn custom-btn d-inline-block increase">+</a>
                                    <input type="number" id="quantity" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric" aria-labelledby="Happy Ninja quantity">
                                    <a href="javascript:void(0)" class="btn custom-btn d-inline-block decrease">-</a>
                                </div>
                                <a href="javascript:void(0);" class="btn add-to-cart text-uppercase button product_type_variable add_to_cart_button ajax_add_to_cart cus_ajax_add_to_cart custom-btn" data-pid="<?php echo $_product->get_id();?>" data-vid="<?php echo $variation_id;?>" data-attribute="<?php echo htmlspecialchars(json_encode($default), ENT_QUOTES, 'UTF-8'); ?>">Add to cart</a>
                            </form>
                        <?php endif;?>
                        <?php else:?>
                            <a href="<?php echo $_product->add_to_cart_url();?>" class="btn add-to-cart text-uppercase">Add to cart</a>
                        <?php endif;?>

                        <div class="woocommerce-product-details__short-description">
                            <p><?php echo $product->get_description();?></p>
                        </div>

                        <div class="share">
                            <h5 class="black-text text-uppercase d-inline-block">Share now </h5>
                            <ul class="list-inline social d-inline-block">
                                <li class="list-inline-item"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $product->get_permalink();?>"><i class="fa fa-facebook-square fa-2x "></i></a></li>
                                <li class="list-inline-item"><a href="https://twitter.com/home?status=<?php echo $product->get_permalink();?>"><i class="fa fa-twitter fa-2x"></i></a></li>
                                <li class="list-inline-item"><a href="https://www.instagram.com/?url=<?php echo $product->get_permalink();?>"><i class="fa fa-instagram fa-2x"></i></a></li>
                                <li class="list-inline-item"><a href="mailto:?subject=<?php echo $product->get_title();?>&amp;body=Check out this product <?php echo $product->get_permalink();?>"><i class="fa fa-envelope-o fa-2x"></i></a></li>

                            </ul>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
</section>

<section class="product-review">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-8 col-sm-12 m-auto">
                <div class="row align-items-center top-section">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <?php
                        if($product->get_review_count() > 0):
                        ?>
                        <h4 class="mb-lg-0 mb-3">Product Reviews</h4>
                        <?php else:?>
                            <h4 class="mb-lg-0 mb-3">No Review Added Yet!</h4>
                        <?php endif;?>
                    </div>
                    <?php if(!is_user_logged_in()):?>
                    <div class="col-lg-6 col-md-6 col-sm-12 addReview ">
                        <a href="<?php bloginfo('url');?>/login" class="btn custom-btn addreviewbtn">ADD REVIEW</a>
                    </div>
                    <?php endif;?>
                </div>
                <?php
                if($product->get_review_count() > 0):
                ?>
                <div class="review-block mb-3">
                    <?php
                    $args = array(
                        'post_id'      => $product->get_id(),
                        'status'      => 'approve',
                        'post_status' => 'publish',
                        'post_type'   => 'product'
                    );

                    $comments = get_comments( $args );

                    foreach ($comments as $comment):
                    $rating = get_comment_meta( $comment->comment_ID, 'rating', true );
                    ?>
                    <div class="media d-none">
                        <img src="<?php echo get_avatar_url($comment->comment_author_email);?>" alt="<?php echo $comment->comment_author;?>" class="mr-3 mt-3 rounded-circle"
                             style="width:60px;">
                        <div class="media-body review-box">
                            <h5 class="d-inline-block"><?php echo $comment->comment_author;?>
                                <small><?php echo date('F j, Y',strtotime($comment->comment_date));?></small>
                            </h5>
                            <ul class="list-inline d-inline-block">
                                <?php
                                for($i = 1; $i <= 5; $i++){
                                    if($i <= $rating){
                                        echo '<li class="list-inline-item active"><a href="javascript:void(0);"><i class="fa fa-star-o"></i></a></li>';
                                    }else{
                                        echo '<li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-star-o"></i></a></li>';
                                    }
                                }
                                ?>
                            </ul>
                            <p><?php echo $comment->comment_content;?></p>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <?php if($product->get_review_count() > 5):?>
                    <div class="text-center">
                        <a href="javascript:void(0)" class="btn load-more text-uppercase">Load More</a>
                    </div>
                    <?php endif;?>
                </div>
                <?php endif;?>
            </div>
            <?php if(is_user_logged_in()):?>
            <div class="col-lg-6 col-md-8 col-sm-12 add-review-form m-auto ">
                <form name="review_frm" id="review_frm">
                    <div class="alert alert-success d-none"></div>
                    <div class="alert alert-danger d-none"></div>
                    <fieldset>Here you can review this item.</fieldset>
                    <div class="form-group ">
                        <input type="text" class="form-control" id="reviewer_name" name="reviewer_name"  placeholder="Your name">
                    </div>
                    <div class="form-group ">
                        <input type="email" class="form-control" id="reviewer_email" name="reviewer_email" placeholder="Your email">
                    </div>
                    <div class="form-group">
                        <textarea  rows="3" class="form-control" id="comment" name="comment" placeholder="Your review"></textarea>
                    </div>
                    <div class="review float-lg-left float-md-left float-sm-none text-center mb-3 sub-review">
                        <ul class="list-inline d-inline-block">
                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-star-o"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-star-o"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-star-o "></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-star-o "></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void(0);"><i class="fa fa-star-o "></i></a></li>
                        </ul>
                    </div>
                    <div class="sub-btn float-lg-right float-md-right float-sm-none">
                        <button type="submit" id="comment_sub" class="btn custom-btn">add review</button>
                        <input type="hidden" name="comment_post_ID" value="<?php echo $product->get_id();?>" id="comment_post_ID">
                        <input type="hidden" name="rate_value" value="" id="rate_value">
                        <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                    </div>
                    <div class="clearfix"></div>

                </form>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>

<div class="related-product">
    <div class="container">
        <div class="row">
            <div class="col-12 no-orders">
                <?php
                $related_ids = wc_get_related_products($product->get_id(),4);
                //echo '<pre>';
                //print_r($related_ids);
                //echo '</pre>';
                /*$args = array(
                    'post_type'             => 'product',
                    'post_status'           => 'publish',
                    'posts_per_page'        => 4,
                    'post__not_in'   => array( $product->get_id() ),
                    'tax_query'             => array(
                        array(
                            'taxonomy'      => 'product_cat',
                            'field' => 'term_id',
                            'terms'         => $trm_id,
                            'operator'      => 'IN'
                        )
                    )
                );
                $products = new WP_Query($args);

                if ( $products->have_posts() ) :*/
                if ( !empty($related_ids) ) :
                ?>
                <div class="title text-center">
                    <h4 class="text-uppercase black-text">More <?php echo $trm_name;?></h4>
                    <a href="<?php echo $link;?>" class="text-uppercase">see all</a>
                </div>
                <ul class="products columns-4">
                    <?php
                    //while ( $products->have_posts() ) : $products->the_post();
                    foreach ($related_ids as $r_id):
                    //global $product;
                        $product = wc_get_product( $r_id );
                    ?>
                        <li class="post-<?php echo $product->get_id();?> product type-product status-publish entry product-type-<?php echo $product->get_type();?>">
                        <a href="<?php echo $product->get_permalink();?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                            <?php echo $product->get_image('woocommerce_thumbnail');?>
                            <h6 class="woocommerce-loop-product__title text-center font-weight-bold"><?php echo $product->get_name();?></h6>
                            <div class="no-orders-content">
                                <?php if($product->get_type() == 'variable'):
                                    $variations = $product->get_available_variations();

                                    if(!empty($variations)):
                                        $attributes = $_product->get_variation_attributes();

                                        foreach ($attributes as $key => $value):
                                            $terms = wc_get_product_terms( $_product->get_id(), $key, array( 'fields' => 'all' ) );
                                            $narr = [];
                                            if(!empty($terms)){
                                                foreach ($terms as $term):
                                                    $narr[$term->slug] = $term->name;
                                                endforeach;
                                            }
                                        endforeach;

                                        ?>
                                        <ul class="list-inline variation text-center">
                                            <?php foreach ($variations as $key => $value):?>
                                                <?php if(is_array($value)):?>
                                                    <li class="list-inline-item">
                                                        <p><?php echo $narr[$value['attributes']['attribute_pa_size']];?></p>
                                                        <p class="font-weight-bold"><?php echo get_woocommerce_currency_symbol();?><?php echo $value['display_price'];?></p>
                                                    </li>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        </ul>
                                    <?php endif;?>
                                <?php endif;?>
                            </div>
                        </a>
                        <div class="shop-btn">
                            <a href="<?php echo $product->get_permalink();?>" class="btn custom-btn text-uppercase line-btn shop-now-btn">Shop Now</a>
                        </div>
                    </li>
                    <?php endforeach;//endwhile; ?>
                    <?php //wp_reset_query(); ?>
                </ul>
                <?php else : ?>
                    <div class="title text-center">
                        <h4 class="text-uppercase black-text">No More <?php echo $trm_name;?></h4>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>