<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
//do_action( 'woocommerce_account_navigation' ); ?>

<!--<div class="woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		//do_action( 'woocommerce_account_content' );
	?>
</div>-->

<!-- /. no.orders-section start  --->
<section class="orders-section">
    <div class="container"> <!-- /.container start  -->
        <!-- /.row start  --->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <p class="">Home / My account</p>
                <h4 class="green-text">Orders</h4>
            </div>
        </div>    <!-- /.row end  -->
    </div>         <!-- /.container end  -->
</section><!-- /.no-orders-section end  -->
<!-- /. no-orders-box start  --->
<?php
//echo '<pre>';
//print_r(array_keys(wc_get_order_statuses()));
//echo '</pre>';
$customer_orders = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
    'numberposts' => $order_count,
    'meta_key'    => '_customer_user',
    'meta_value'  => get_current_user_id(),
    'post_type'   => wc_get_order_types( 'view-orders' ),
    'post_status' => array_keys( wc_get_order_statuses() ),
) ) );

$customer_orders_completes = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
    'numberposts' => $order_count,
    'meta_key'    => '_customer_user',
    'meta_value'  => get_current_user_id(),
    'post_type'   => wc_get_order_types( 'view-orders' ),
    'post_status' => 'wc-completed',
) ) );

$customer_orders_processing = get_posts( apply_filters( 'woocommerce_my_account_my_orders_query', array(
    'numberposts' => $order_count,
    'meta_key'    => '_customer_user',
    'meta_value'  => get_current_user_id(),
    'post_type'   => wc_get_order_types( 'view-orders' ),
    'post_status' => 'wc-processing',
) ) );

?>


<section class="orders-box">
    <div class="nav-tab">
        <div class="container">
            <?php if ( $customer_orders ) : ?>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs float-left" role="tablist">
                <li><a href="#all" role="tab"  class="active" data-toggle="tab">ALL</a></li>
                <li><a href="#delivered" role="tab" data-toggle="tab">DELIVERED</a></li>
                <li><a href="#processed" role="tab" data-toggle="tab">PROCESSED</a></li>
            </ul>
            <?php endif;?>
            <?php
            global  $current_user;
            $full_name = get_user_meta($current_user->ID,'full_name',true);
            $image_url = get_user_meta($current_user->ID,'photo_id',true);
            if(empty($image_url)){
                $image_url = get_avatar_url( $current_user->ID );
            }
            if(empty($full_name)){
                $full_name = $current_user->display_name;
            }
            ?>
            <ul class="nav nav-tabs float-right user-account" role="tablist">
                <li class="nav-item dropdown ">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <img src="<?php echo esc_url( $image_url ); ?>" width="33" height="33" alt="" class="mr-2 rounded-circle">
                        <span><?php echo $full_name;?></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="<?php bloginfo('url');?>/my-account/edit-account/"><i class="fa fa-user" aria-hidden="true"></i> My Account</a>
                        <a class="dropdown-item edit-profile-picture" href="javascript:void(0);"><i class="fa fa-cog" aria-hidden="true"></i> Profile Picture</a>
                        <a class="dropdown-item" href="<?php echo wp_logout_url(); ?>"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a>
                    </div>
                </li>
            </ul>
            <div class="clearfix"></div>
            <!-- Nav tabs -->
        </div>
    </div>
</section>
<!-- /. no-orders-tab end --->
<?php if ( empty($customer_orders) ) : ?>
<!-- /. no-order-image-box start  --->
<section class="recommended-product">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p>No Orders Yet</p>
                <h6 class="border-bottom">Let Us Recommend</h6>
            </div>
            <?php
            $recommend_products = get_field('recommend_products', 'option');
            if(!empty($recommend_products)):?>
            <div class="col-12 no-orders">
                <ul class="products columns-4">
                    <?php foreach ($recommend_products as $recommend_product):
                        $product = wc_get_product( $recommend_product->ID );
                        ?>
                        <li class="post-<?php echo $product->get_id();?> product type-product status-publish entry product-type-<?php echo $product->get_type();?>">
                            <a href="<?php echo $product->get_permalink();?>" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                                <?php echo $product->get_image('woocommerce_thumbnail');?>
                                <h6 class="woocommerce-loop-product__title text-center font-weight-bold"><?php echo $product->get_name();?></h6>
                                <div class="no-orders-content">
                                    <?php if($product->get_type() == 'variable'):
                                        $variations = $product->get_available_variations();

                                        if(!empty($variations)):
                                            $attributes = $product->get_variation_attributes();

                                            foreach ($attributes as $key => $value):
                                                $terms = wc_get_product_terms( $product->get_id(), $key, array( 'fields' => 'all' ) );
                                                $narr = [];
                                                if(!empty($terms)){
                                                    foreach ($terms as $term):
                                                        $narr[$term->slug] = $term->name;
                                                    endforeach;
                                                }
                                            endforeach;

                                            ?>
                                            <ul class="list-inline variation text-center">
                                                <?php foreach ($variations as $key => $value):?>
                                                    <?php if(is_array($value)):?>
                                                        <li class="list-inline-item">
                                                            <p><?php echo $narr[$value['attributes']['attribute_pa_size']];?></p>
                                                            <p class="font-weight-bold"><?php echo get_woocommerce_currency_symbol();?><?php echo $value['display_price'];?></p>
                                                        </li>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </ul>
                                        <?php endif;?>
                                    <?php endif;?>
                                </div>
                            </a>
                            <div class="shop-btn">
                                <a href="<?php echo $product->get_permalink();?>" class="btn custom-btn text-uppercase line-btn shop-now-btn">Shop Now</a>
                            </div>
                        </li>
                    <?php endforeach;?>
                </ul>


            </div>
            <?php endif;?>
        </div>
    </div>
</section>
<!-- /. no-order-image-box end  --->
<?php endif;?>
<?php if ( $customer_orders ) : ?>
<section class="order-content-box">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="all">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th >NO</th>
                                    <!--<th >DESCRIPTION</th>-->
                                    <th >STATUS</th>
                                    <th >DATE</th>
                                    <th colspan="2" >VALUE</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $customer_orders as $customer_order ) :
                                    $order      = wc_get_order( $customer_order );
                                    $item_count = $order->get_item_count();
                                    $order_date = date('m/d/Y',strtotime($order->order_date));
                                    ?>
                                    <tr>
                                        <td><p><?php echo $order->get_order_number();?></p></td>
                                        <!--<td><p><?php echo $order->customer_note;?></p></td>-->
                                        <td class="status"><p><?php echo ucfirst($order->get_status());?></p></td>
                                        <td><p><?php echo $order_date;?></p></td>
                                        <td><p><?php echo $order->get_formatted_order_total();?> for <?php echo $item_count;?> items</p></td>
                                        <td><p><a href="<?php echo $order->get_view_order_url();?>" class="btn-view-order">view order</a></p> </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane" id="delivered">
                        <div class="table-responsive">
                            <?php if(!empty($customer_orders_completes)):?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>NO</th>
                                    <!--<th>DESCRIPTION</th>-->
                                    <th>STATUS</th>
                                    <th>DATE</th>
                                    <th colspan="2">VALUE</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $customer_orders_completes as $customer_order ) :
                                    $order      = wc_get_order( $customer_order );
                                    $item_count = $order->get_item_count();
                                    $order_date = date('m/d/Y',strtotime($order->order_date));
                                    ?>
                                    <tr>
                                        <td><p><?php echo $order->get_order_number();?></p></td>
                                        <!--<td><p><?php echo $order->customer_note;?></p></td>-->
                                        <td class="status"><p><?php echo ucfirst($order->get_status());?></p></td>
                                        <td><p><?php echo $order_date;?></p></td>
                                        <td><p><?php echo $order->get_formatted_order_total();?> for <?php echo $item_count;?> items</p></td>
                                        <td><p><a href="<?php echo $order->get_view_order_url();?>" class="btn-view-order">view order</a></p> </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else:?>
                                <p>Nothings Found!</p>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="tab-pane" id="processed">
                        <div class="table-responsive">
                            <?php if(!empty($customer_orders_processing)):?>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>NO</th>
                                    <!--<th>DESCRIPTION</th>-->
                                    <th>STATUS</th>
                                    <th>DATE</th>
                                    <th colspan="2">VALUE</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ( $customer_orders_processing as $customer_order ) :
                                    $order      = wc_get_order( $customer_order );
                                    $item_count = $order->get_item_count();
                                    $order_date = date('m/d/Y',strtotime($order->order_date));
                                    ?>
                                    <tr>
                                        <td><p><?php echo $order->get_order_number();?></p></td>
                                        <!--<td><p><?php echo $order->customer_note;?></p></td>-->
                                        <td class="status"><p><?php echo ucfirst($order->get_status());?></p></td>
                                        <td><p><?php echo $order_date;?></p></td>
                                        <td><p><?php echo $order->get_formatted_order_total();?> for <?php echo $item_count;?> items</p></td>
                                        <td><p><a href="<?php echo $order->get_view_order_url();?>" class="btn-view-order">view order</a></p> </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <?php else:?>
                                <p>Nothings Found!</p>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;?>