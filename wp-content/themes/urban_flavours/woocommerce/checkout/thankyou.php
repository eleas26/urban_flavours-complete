<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<!-- sing up top section -->
<div class="sing-up-top ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="sing-up-nvigation cart-nav">
                    <ul>
                        <li><a href="javascript:void(0);" title="Cart">SHOPPING CART</a></li>
                        <li><a href="javascript:void(0);" title="Checkout" >CHECKOUT</a></li>
                        <li><a href="javascript:void(0);" title="Complete order" class="active">ORDER COMPLETED</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- sing up top section -->

<!-- cart start section -->

<div class="order-complted-section">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="cart-order ">
                    <?php if ( $order ) : ?>
                        <?php if ( $order->has_status( 'failed' ) ) : ?>
                            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

                            <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
                                <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
                                <?php if ( is_user_logged_in() ) : ?>
                                    <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
                                <?php endif; ?>
                            </p>
                        <?php else : ?>
                            <a href="javascript:void(0);"><img src="<?php bloginfo('template_url');?>/images/cart.png" class="pt-3" alt="urban-upload-img"></a>
                            <h3>Order Complete</h3>
                            <p>You order is complete ! Your order number is <span class="cart-text"><?php echo $order->get_order_number(); ?></span> and thank you for shopping at Urban Flavours</p>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="continue-shopping text-center">
                    <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) );?>" class="text-center continue-shopping-btn">CONTINUE SHOPPING</a>
                </div>
            </div>
        </div>
    </div>
</div>
