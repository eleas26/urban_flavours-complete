<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

?>
<div class="registration-wrapper">
    <!-- sing up top section -->
    <div class="sing-up-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <nav class="sing-up-nvigation">

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class=" nav-link active" title="Registration" href="#edit-registration" role="tab" data-toggle="tab" disabled="disabled">EDIT REGISTRATION</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#edit_upload_id" role="tab" data-toggle="tab" disabled="disabled">EDIT UPLOAD ID</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#upload_photo" role="tab" data-toggle="tab" disabled="disabled">EDIT PHOTO</a>
                            </li>
                        </ul>
                    </nav>
                    <!-- Tab panes -->
                </div>
            </div>
        </div>
    </div>
    <!-- sing up top section -->

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade show active" id="edit-registration">
            <?php
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
            $current_user_metadata = get_user_meta($user_id);

            $front_side = $current_user_metadata['front_side'][0];
            $back_side = $current_user_metadata['back_side'][0];
            $medical_id = $current_user_metadata['medical_id'][0];
            $photo_id = $current_user_metadata['photo_id'][0];
            ?>
            <!-- sing up section start here -->
            <form class="sing-up-section edit-user-info">
                <div class="container">
                    <div class="row pb-2">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="sing-in-left">
                                <div class="row align-items-center">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user_metadata['first_name'][0])){ echo 'validation-success';}?>">
                                            <label for="text">First Name*</label>
                                            <input type="text" class="form-control sing-up" id="text" aria-describedby="textHelp" name="first-name" value="<?php if(!empty($current_user_metadata['first_name'][0])){ echo $current_user_metadata['first_name'][0];}?>">
                                            <span class="error-span">Please provide your first name!</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user_metadata['last_name'][0])){ echo 'validation-success';}?>">
                                            <label for="text1">Last Name*</label>
                                            <input type="text" class="form-control sing-up" id="text1" aria-describedby="text1Help" name="last-name" value="<?php if(!empty($current_user_metadata['last_name'][0])){ echo $current_user_metadata['last_name'][0];}?>">
                                            <span class="error-span">Please provide your last name!</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user->user_email)){ echo 'validation-success';}?>">
                                            <label for="email">Email*</label>
                                            <input type="email" class="form-control sing-up" id="email" aria-describedby="emailHelp" name="email" value="<?php if(!empty($current_user->user_email)){ echo $current_user->user_email;}?>">
                                            <span class="error-span" data-invalid="Invalid Email!" data-ajax="Your email is not available">Invalid Email</span>
                                            <div class="yloader"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="password">Password*</label>
                                            <input type="password" class="form-control sing-up" id="password" aria-describedby="emailHelp" name="user-password" value="">
                                            <span class="error-span">Please provide your Password!</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="confrim-pass">Confirm Password*</label>
                                            <input type="password" class="form-control sing-up" id="confrim-pass" aria-describedby="emailHelp" name="confirm-pass" value="">
                                            <span class="error-span">Password Does not match..!</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <div class="form-check validation_required <?php if(!empty($current_user->user_email)){ echo 'validation-success';}?>">
                                                <input class="form-check-input" type="checkbox" id="terms" <?php if(!empty($current_user->user_email)){ echo 'checked';}?>>
                                                <label class="form-check-label sing-up-check" for="terms">
                                                    I agree the <a href="#">terms and conditions</a>
                                                    <span class="error-span">You must agree to our Terms and Conditions to Continue.</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="sing-in-right">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user_metadata['birth_date'][0])){ echo 'validation-success';}?>">
                                            <label for="datepicker">Date of Birth*</label>
                                            <input type="text" class="form-control sing-up" id="datepicker" aria-describedby="emailHelp" placeholder="MM / DD / YYYY" name="birth-date" value="<?php if(!empty($current_user_metadata['birth_date'][0])){ echo $current_user_metadata['birth_date'][0];}?>">
                                            <span class="error-span">You must be 18+</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user_metadata['user_phone'][0])){ echo 'validation-success';}?>">
                                            <label for="number">Phone Number*</label><div class="float-right"><input class="form-check-input" type="checkbox" id="gridCheck" name="allow_text_message" <?php if(isset($current_user_metadata['allow_text_message'][0]) && ($current_user_metadata['allow_text_message'][0] == 1 || $current_user_metadata['allow_text_message'][0] == 'on')){ echo 'checked';}?>>
                                                <label class="form-check-label sing-up-check" for="gridCheck">Allow SMS?
                                                </label></div>
                                            <input type="text" class="form-control sing-up" id="number" aria-describedby="emailHelp" name="phone" value="<?php if(!empty($current_user_metadata['user_phone'][0])){ echo $current_user_metadata['user_phone'][0];}?>">
                                            <span class="error-span">Please provide a valid phone number!</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user_metadata['zip_code'][0])){ echo 'validation-success';}?>">
                                            <label for="zipcode">ZIP Code*</label>
                                            <input type="text" class="form-control sing-up" id="zipcode" aria-describedby="emailHelp" name="zip-code" value="<?php if(!empty($current_user_metadata['zip_code'][0])){ echo $current_user_metadata['zip_code'][0];}?>">
                                            <span class="error-span" data-invalid="Please provide a zip in the format #####" data-ajax="Sorry, our service is not available in your area.">Please provide a zip in the format #####</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group validation_required <?php if(!empty($current_user_metadata['address'][0])){ echo 'validation-success';}?>">
                                            <label for="address">Address*</label>
                                            <input type="text" class="form-control sing-up" id="address" aria-describedby="emailHelp" name="address" value="<?php if(!empty($current_user_metadata['address'][0])){ echo $current_user_metadata['address'][0];}?>">

                                            <span class="error-span">Address field is too short!</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group text-center mt-4">
                                            <button type="submit" id="singBtn" class="btn sing-up-btn su-btn-next text-uppercase" data-id="edit_upload_id">NEXT</button>
                                            <input type="hidden" name="edit_user_id" id="edit_user_id" value="<?php echo $user_id;?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="edit_upload_id">

            <!-- sing up upload-id section -->
            <div class="upload-section pb-5">
                <div class="container">
                    <div class="row text-center">
                        <div class="col-lg-6 col-md-8 col-sm-7  ml-auto">
                            <div class="upload-left-side py-5 px-5 font-side-upload" id="dz_upload1">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/images/urban-upload.png" class="pt-3" alt="urban-upload-img">
                                <h5 class="text-uppercase">UPLOAD Font Side</h5>
                                <p class="pt-2">Drag and drop photo here or just click to <span class="font-side-btn green-text font-weight-bold">browse</span> files  </p>
                            </div>

                            <div class="upload-left-side py-5 px-5 back-side-upload mt-3" id="dz_upload2">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/images/urban-upload.png" class="pt-3" alt="urban-upload-img">
                                <h5 class="text-uppercase">UPLOAD Back Side</h5>
                                <p class="pt-2">Drag and drop photo here or just click to <span class="back-side-btn green-text font-weight-bold">browse</span> files  </p>
                            </div>

                            <p id="dz_upload1_err" class="<?php if(empty($front_side) || empty($back_side)){?>validation_required<?php };?>"><span class="error-span">Please Upload both Front &amp; Back Side of your ID</span></p>

                            <div class="upload-left-side py-5 px-5 back-side-upload" id="dz_upload3">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/images/urban-upload.png" class="pt-3" alt="urban-upload-img">
                                <h5 class="text-uppercase">UPLOAD Medical ID</h5>
                                <p class="pt-2">Drag and drop photo here or just click to <span class="back-side-btn green-text font-weight-bold">browse</span> files  </p>
                            </div>

                            <p id="dz_upload3_err" class=""><span class="error-span">Please Upload your Medical ID</span></p>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-5 edit-wrap" id="dz_preview_wrap">

                            <div class="preview_container_front dropzone-previews" id="preview_container_front">
                                <div class="upload-right-side dash-border align-items-center" >
                                    <div class="box">
                                        <?php
                                        if(!empty($front_side)){
                                            echo '<img src="'.$front_side.'"/>';
                                        }
                                        ?>
                                        <h5>Front SIDE</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="preview_container_back dropzone-previews" id="preview_container_back">
                                <div class="upload-right-side dash-border align-items-center  mt-2" >
                                    <div class="box">
                                        <?php
                                        if(!empty($back_side)){
                                            echo '<img src="'.$back_side.'"/>';
                                        }
                                        ?>
                                        <h5>BACK SIDE</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="preview_container_mid dropzone-previews" id="preview_container_mid">
                                <div class="upload-right-side dash-border align-items-center  mt-2" >
                                    <div class="box">
                                        <?php
                                        if(!empty($medical_id)){
                                            echo '<img src="'.$medical_id.'"/>';
                                        }
                                        ?>
                                        <h5>MEDICAL ID</h5><p>+if needed</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 mt-5">
                            <div class="upload-button">
                                <button type="submit" class="btn upload-back-btn pull-left su-btn" data-id="edit-registration"><i aria-hidden="true" class="arrow_carrot-left"></i>BACK</button>
                                <button type="submit" class="btn upload-next-btn su-btn-next float-right" data-id="upload_photo">NEXT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sing up upload-id section -->
        </div>
        <div role="tabpanel" class="tab-pane fade" id="upload_photo">
            <!-- sing up upload-id section -->
            <div class="upload-section pb-5">
                <div class="container">
                    <form action="">
                        <div class="row text-center">
                            <div class="col-lg-6 col-md-8 col-sm-7  ml-auto">
                                <h4 class="success_msg" style="display: none;"></h4>
                                <div class="upload-left-side  upload-photo  py-5 px-5" id="dz_upload4">
                                    <input type="file" name="file" multiple class="d-none">
                                    <img src="<?php echo get_stylesheet_directory_uri();?>/images/urban-upload.png" class="pt-3" alt="urban-upload-img">
                                    <h5 class="text-uppercase">UPLOAD PHOTO</h5>
                                    <p class="pt-2">Drag and drop photo here or just click to <span class="upload-photo-btn green-text font-weight-bold">browse</span> files  </p>
                                </div>
                                <p id="dz_upload4_err" class="<?php if(empty($photo_id)){?>validation_required<?php };?>"><span class="error-span">Please Upload Your Photo</span></p>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-5 edit-wrap photo-edit-wrap">
                                <div class="preview_container_pto dropzone-previews" id="preview_container_pto">
                                    <div class="upload-right-side dash-border align-items-center  mt-2" >
                                        <div class="box">
                                            <?php
                                            if(!empty($photo_id)){
                                                echo '<img src="'.$photo_id.'"/>';
                                            }
                                            ?>
                                            <h5>Your Photo</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 mt-5">
                                <div class="upload-button">
                                    <input type="hidden" name="front_side" id="front_side" value="<?php if(!empty($front_side)){ echo $front_side;}?>">
                                    <input type="hidden" name="back_side" id="back_side" value="<?php if(!empty($back_side)){ echo $back_side;}?>">
                                    <input type="hidden" name="medical_id" id="medical_id" value="<?php if(!empty($medical_id)){ echo $medical_id;}?>">
                                    <input type="hidden" name="photo_id" id="photo_id" value="<?php if(!empty($photo_id)){ echo $photo_id;}?>">
                                    <input type="hidden" name="create_user" value="1">

                                    <button type="submit" class="btn su-btn upload-back-btn" data-id="edit_upload_id"><i aria-hidden="true" class="arrow_carrot-left"></i>BACK</button>
                                    <button type="submit" class="btn upload-next-btn float-right" id="edit_account">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- sing up upload-id section -->
        </div>
    </div>
</div>

<div class="wait wait-reg"></div>