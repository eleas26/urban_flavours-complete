<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package urban_flavours
 */

get_header();
?>
    <?php
        $slider_settings = get_field('slider_settings', 'option');
        if(!empty($slider_settings)):
    ?>
        <!-- /.slider-area start  -->
        <section class="slider-area slider">
            <?php foreach ($slider_settings as $slider_setting):?>
            <div>
                <div style="background-image: url('<?php echo $slider_setting['slider_image'];?>');" class="slide">
                    <div class="caption d-flex h-100 align-items-center justify-content-center">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-12 text-center ">
                                    <h1 class="white-text"><?php echo $slider_setting['slider_heading'];?></h1>
                                    <h4 class="white-text mb-60">
                                        <?php echo $slider_setting['slider_sub_heading'];?>
                                    </h4>
                                    <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) );?>" class="btn custom-btn text-uppercase">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </section>
        <!-- /.slider-area end  -->
    <?php endif;?>
    <?php
    $deal_of_the_day_product = get_field('deal_of_the_day_product', 'option');
    $button_text = get_field('button_text', 'option');

    if(!empty($deal_of_the_day_product)):
        $_product = wc_get_product( $deal_of_the_day_product->ID );
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $_product->get_id() ), 'full' );
    ?>
    <!-- /.deal-area start  -->
    <section class="deal-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 ">
                    <div class="deal-box">
                        <h2 class="text-uppercase green-text">Deal of the day</h2>
                        <?php if(!empty($image[0])):?>
                        <img src="<?php  echo $image[0]; ?>" alt="<?php echo $_product->get_name();?>">
                        <?php endif;?>
                    </div>
                    <div class="deal-info-box text-center">
                        <?php
                        $catIds = $_product->get_category_ids();
                        $category_name = [];
                        $term_url = '';
                        if (!empty($catIds)) {
                            $nofcat = count($catIds);
                            if($nofcat > 1){
                                foreach ($catIds as $cat_id) {
                                    $trm = get_term_by('id', $cat_id, 'product_cat');
                                    if($trm->parent != 0) {
                                        $category_name[] = $trm->name;
                                        $term_url = get_term_link($trm);
                                        break;
                                    }
                                }
                            }else{
                                $trm = get_term_by('id', $catIds[0], 'product_cat');
                                $term_url = get_term_link($trm);
                                $category_name[] = $trm->name;
                            }
                        }

                        ?>
                        <div class="cat">
                            <h6 class="text-uppercase text-center black-text"><?php echo implode(', ', $category_name);?></h6>
                        </div>
                        <div class="top-title ">
                            <h3 class="text-uppercase black-text "><?php echo $_product->get_name();?></h3>
                        </div>
                        <p class="black-text"><?php echo $_product->get_short_description();?></p>
                        <?php if($_product->get_type() == 'variable'):
                            $variations = $_product->get_available_variations();
                            //echo '<pre>';
                            //print_r($variations);
                            //echo '</pre>';
                            if(!empty($variations)):
                                $attributes = $_product->get_variation_attributes();
                                $default = $_product->get_default_attributes();
                                //echo '<pre>';
                                //print_r($attributes);
                                //echo '</pre>';

                                //$attribute_name = array_keys($attributes);

                                //echo '<pre>';
                                //print_r($attribute_name);
                                //echo '</pre>';

                                foreach ($attributes as $key => $value):
                                    $terms = wc_get_product_terms( $_product->get_id(), $key, array( 'fields' => 'all' ) );
                                    $narr = [];
                                    if(!empty($terms)){
                                        foreach ($terms as $term):
                                            $narr[$term->slug] = $term->name;
                                        endforeach;
                                    }
                                endforeach;

                                $variation_id = cus_find_matching_product_variation( $_product, $default );
                            ?>
                            <ul class="list-inline variation">
                                <?php //foreach ($attributes as $key => $value):?>
                                <?php foreach ($variations as $key => $value):?>
                                <?php if(is_array($value)):?>
                                    <?php
                                        $attr_arr=[];
                                        $attr_arr['pa_size'] = $value['attributes']['attribute_pa_size'];
                                    ?>
                                    <?php //foreach ($value as $itm => $val):?>
                                        <li class="list-inline-item <?php if(in_array($value['attributes']['attribute_pa_size'],$default)){?>active<?php };?>" id="vid-<?php echo $value['variation_id'];?>">
                                            <p data-variation_id="<?php echo $value['variation_id'];?>" data-product_id="<?php echo $_product->get_id();?>" data-attri="attribute_pa_size" data-attri_val="<?php echo $value['attributes']['attribute_pa_size'];?>" data-attribute="<?php echo htmlspecialchars(json_encode($attr_arr), ENT_QUOTES, 'UTF-8'); ?>"><?php echo $narr[$value['attributes']['attribute_pa_size']];?></p>
                                            <p data-variation_id="<?php echo $value['variation_id'];?>" data-product_id="<?php echo $_product->get_id();?>" data-attri="attribute_pa_size" data-attri_val="<?php echo $value['attributes']['attribute_pa_size'];?>" data-attribute="<?php echo htmlspecialchars(json_encode($attr_arr), ENT_QUOTES, 'UTF-8'); ?>"><?php echo get_woocommerce_currency_symbol();?><?php echo $value['display_price'];?></p>
                                        </li>
                                        <?php //endforeach;?>
                                <?php endif;?>
                                <?php endforeach;?>
                            </ul>
                            <?php endif;?>
                            <!--<a href="<?php echo get_bloginfo('url');?>/?add-to-cart=<?php echo $variation_id;?>" class="btn add-to-cart text-uppercase button product_type_variable add_to_cart_button ajax_add_to_cart home_cart_url" disabled="disabled">Add to cart</a>-->
                            <a href="javascript:void(0);" class="btn add-to-cart text-uppercase button product_type_variable add_to_cart_button ajax_add_to_cart cus_ajax_add_to_cart home_cart_url" data-pid="<?php echo $_product->get_id();?>" data-vid="<?php echo $variation_id;?>" data-attribute="<?php echo htmlspecialchars(json_encode($default), ENT_QUOTES, 'UTF-8'); ?>" disabled="disabled">Add to cart</a>
                            <?php
                            global $woocommerce;
                            $cart_url = $woocommerce->cart->get_cart_url();
                            if(isset($_GET['add-to-cart'])){
                            ?>
                            <a href="<?php echo $cart_url;?>" class="d-inline-block ml-2 black-text viw-crd">View cart</a>
                            <?php };?>
                        <?php else:?>
                            <p><?php echo $_product->get_price_html();?></p>
                            <a href="<?php echo $_product->add_to_cart_url();?>" class="btn add-to-cart text-uppercase">Add to cart</a>
                        <?php endif;?>
                    </div>
                    <div class="more-btn text-center">
                        <a href="<?php echo $term_url;?>" class="btn custom-btn text-uppercase"><?php echo $button_text;?></a>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /.deal-area end  -->
    <?php endif;?>
    <?php
    $service_section_image = get_field('service_section_image', 'option');
    $service_section_map_code = get_field('service_section_map_code', 'option');
    $service_section_title = get_field('service_section_title', 'option');
    $service_section_short_text = get_field('service_section_short_text', 'option');
    $service_section_button_text = get_field('service_section_button_text', 'option');
    ?>
    <!-- /.service-area start  -->
    <section class="service-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12  p-lg-0 p-md-0 p-sm-1">
                    <div class="service-box">
                        <img src="<?php echo $service_section_image;?>" alt="service">
                    </div>
                    <div class="service-nfo-box">
                        <div class="nfo-box">
                            <h2 class="green-text text-uppercase"><?php echo $service_section_title;?></h2>
                            <p><?php echo $service_section_short_text;?></p>
                            <a href="<?php bloginfo('url');?>/service-areas/" class="btn learn-more text-uppercase"><?php echo $service_section_button_text;?></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 p-lg-0 p-md-0 p-sm-1">
                    <div class="service-map " id="map"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.service-area end  -->
    <?php
    $home_page_product_categories = get_field('home_page_product_categories', 'option');
    if(!empty($home_page_product_categories)):
    ?>
    <!-- /.categories-area start  -->
    <section class="categories-area">
        <div class="container-fluid no-gutters">
            <div class="row">
                <?php foreach ($home_page_product_categories as $home_page_product_category):
                    $term = get_term_by( 'id', $home_page_product_category, 'product_cat' );
                    $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                    // get the image URL
                    $image = wp_get_attachment_url( $thumbnail_id );
                    $term_url = get_term_link($home_page_product_category);
                    ?>
                <div class="col-lg-3 col-md-3 col-sm-6 p-0 cat-box">
                    <a href="<?php echo $term_url;?>">
                        <div class="category text-center overlay">
                            <img src="<?php echo $image;?>" alt="<?php echo $term->name;?>">
                            <h6 class=" text-uppercase"><?php echo $term->name;?></h6>
                        </div>
                    </a>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>
    <!-- /.categories-area end  -->
    <?php endif;?>

    <!-- /.urbanflavours start  -->
    <section class="urbanflavours">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 col-sm-12 ml-auto">
                    <h2 class="green-text text-uppercase title">#urbanflavours</h2>
                </div>
            </div>
        </div>
        <?php
        $instagram_access_token = trim(get_field('instagram_access_token', 'option'));
        $result = fetchData("https://api.instagram.com/v1/users/self/media/recent/?access_token=".$instagram_access_token."&count=20");
        $instagram_result = json_decode($result);
        if(!empty($instagram_result->data)):
        ?>
        <!-- /.urban-slider start  -->
        <div class="urban-slider">
            <div class="container-fluid no-gutters">
                <div class="row">
                    <div class="col-12 p-0">
                        <div class="centrer-slider">
                            <?php foreach ($instagram_result->data as $im):
                                ?>
                            <div>
                                <img src="<?php echo $im->images->standard_resolution->url;?>">
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.urban-slider end  -->
        <?php endif;?>
    </section>
<?php
get_footer();
