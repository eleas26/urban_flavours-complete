<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
    ?>
    <!-- sing up top section -->
    <div class="sing-up-top ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="sing-up-nvigation cart-nav">
                        <ul>
                            <li><a href="javascript:void(0);" title="Cart">SHOPPING CART</a></li>
                            <li><a href="javascript:void(0);" title="Checkout" class="active">CHECKOUT</a></li>
                            <li><a href="javascript:void(0);" title="Complete order">ORDER COMPLETED</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- sing up top section -->
    <section class="checkout-section">
        <div class="container">
            <div class="row">
                <div class="col-12">
    <?php
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	?>
                </div>
            </div>
        </div>
    </section>
                <?php
	return;
}

?>

<!-- sing up top section -->
<div class="sing-up-top ">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="sing-up-nvigation cart-nav">
                    <ul>
                        <li><a href="javascript:void(0);" title="Cart">SHOPPING CART</a></li>
                        <li><a href="javascript:void(0);" title="Checkout" class="active">CHECKOUT</a></li>
                        <li><a href="javascript:void(0);" title="Complete order">ORDER COMPLETED</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- sing up top section -->

<section class="checkout-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <p class="customer"><?php do_action( 'woocommerce_before_checkout_form', $checkout );?></p>
            </div>
        </div>
        <div class="delivery-address">
            <div class="woocommerce">
                <div class="woocommerce-notices-wrapper"></div>
                <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-8 col-md-7 col-sm-12">
                            <?php do_action( 'woocommerce_checkout_billing' ); ?>
                        </div>

                        <div class="col-lg-4 col-md-5 col-sm-12">
                            <div class="woocommerce-billing-fields">
                                <h6 class="black-text text-uppercase text-center order ">Your order</h6>
                                <div id="order_review" class="woocommerce-checkout-review-order">
                                    <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                                </div>
                            </div>

                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>

</section>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
